# README

Contact : cyril.cano@gipsa-lab.fr

## Overview

This Git repo provides the library [mlpgw.py](https://git.ligo.org/cyril.cano/gw-generation/-/tree/master/mlpgw.py)
that allows to train a machine-learning model able to regress gravitational-wave waveform from a set of examples
as described in this [article](https://dcc.ligo.org/LIGO-P2100216). 

This Git repo includes several notebooks that allow to reproduce the results presented in the paper.

The learning part is mainly based on [scikit-learn](https://scikit-learn.org). This package is included in the required
[environment](https://git.ligo.org/cyril.cano/gw-generation/-/blob/master/environment.yml).

Take care that in the notebook a gravitational waveform $`h(t)`$ is denoted $`h_+(t) + i h_\time(t)`$ but $`h_+(t) - i h_\time(t)`$ in the paper.

## Installation

Clone this Git repo and create the environment `gw-generation` by running:

`conda env create -f environment.yml`

Activate the environment 

`conda activate gw-generation`

... and run the following command line from this folder: 

`conda develop .`

## How to generate a waveform using a pre-computed ML model?

This [notebook](https://git.ligo.org/cyril.cano/gw-generation/-/tree/master/notebooks/Prediction.ipynb) shows how to generate a waveform with a pre-computed ML model.

The pre-computed model is stored in a set of Pickle files (see [data/](https://git.ligo.org/cyril.cano/gw-generation/-/tree/master/data))

Important note: the complex-valued gravitational waveform $`h(t)`$ is denoted $`h_+(t) + i h_\time(t)`$ in the notebook while it is $`h_+(t) - i h_\time(t)`$ in the paper (sign change).

## How to train and evaluate a ML model?

This [notebook](https://git.ligo.org/cyril.cano/gw-generation/-/blob/master/notebooks/Train%20and%20Test.ipynb) 
shows how to train the ML model and produces many plots that help to evaluate its performances.

This [notebook](https://git.ligo.org/cyril.cano/gw-generation/-/blob/master/notebooks/Train.ipynb) 
simply shows how to train a model with no plots for faster execution.

## How to generate a training/testing dataset?

To compute the training/testing dataset use the script [create_dataset](https://git.ligo.org/cyril.cano/gw-generation/-/tree/master/create_dataset).

Code usage: 
`python create_dataset.py -s 4000 -n 'data-aligned' -w '{"approximant": "SEOBNRv4"}' -b '{"s1_range": [-0.8,0.95], "s2_range": [-0.8,0.95], "s1": [0,0,1], "s2": [0,0,1], "total_mass_range": [20,20], "mass_ratio_range": [1,20]}' -m '{"name": "bivar2circ", "unwrap": true}' -t '{"n_samples": 4096, "t_merge": 1.0, "power": 0.35, "t_end": 0.0003}' -p ../data/`

A pre-computed dataset can be found on [here](https://filesender.renater.fr/?s=download&token=0ecf40c5-120d-4155-9338-90bfad5591a8).

## Description of the contents of the repo

 - [mlpgw.py](https://git.ligo.org/cyril.cano/gw-generation/-/tree/master/mlpgw.py) includes all routines to manipulate datasets, compute the waveform amplitude and phase and process them
 - [notebooks/](https://git.ligo.org/cyril.cano/gw-generation/-/tree/master/notebooks) contains the notebooks showing how to train and test a model and how to generate a waveform with a pre-computed model
 - [examples/](https://git.ligo.org/cyril.cano/gw-generation/-/tree/master/examples) show examples how to manipulate dataset
 - [create_dataset/](https://git.ligo.org/cyril.cano/gw-generation/-/tree/master/create_dataset) contains codes to generate your own training and testing dataset.
 - [redactions/](https://git.ligo.org/cyril.cano/gw-generation/-/tree/master/redactions) contains an article and presentation about the machine-learning models with principal component regression.
