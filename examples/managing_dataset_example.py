#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 12:07:22 2021

@author: canocy
"""

import numpy as np
import matplotlib.pyplot as plt
import mlpgw
import pandas as pd

# Create a compact binary with default parameters
binary = mlpgw.CompactBinary()

# Create a compact binary with random parameters (default ranges)
binary = mlpgw.CompactBinary().random_parameters()

# Create a compact binary with random parameters but s1=(0,0,2)
binary = mlpgw.CompactBinary().random_parameters(s1=(0,0,1), s1_range=(0.9,0.9))

# Get total mass of the compact binary
print(binary.s1x, binary.s1y, binary.s1z)

# Get all compact binary parameters
print(binary.get_parameters())
# Or
print(binary.get_parameters(['total_mass']))
# Or
print(binary.total_mass())

# Compute waveform
wvf = binary.get_waveform(inclination=np.pi/2)

# Data should always be a dict with a key 'time'
print(wvf.keys())

plt.figure()
plt.plot(wvf['time'], wvf['hp'])
plt.plot(wvf['time'], wvf['hc'])

# Create a transform associated with bivar2circ
transformer = mlpgw.Transformer(name='bivar2circ', unicity=False, unwrap=True)

# Apply the transform to the waveform
circ = transformer.transform(**wvf)

print(circ.keys())

# Apply inverse transform
wvf_rec = transformer.inverse_transform(**circ)

plt.figure()
plt.plot(wvf['time'], wvf['hp'])
plt.plot(wvf['time'], wvf['hc'])
plt.plot(wvf_rec['time'], wvf_rec['hp'])
plt.plot(wvf_rec['time'], wvf_rec['hc'])

# Create a set of binaries randomly
binaryset, info = mlpgw.random_binaryset(size=1, s1=(0,0,1), s1_range=(0.9,0.9))

nominal_time = mlpgw.get_nominal_time(n_samples=2048)

# Create a resampler (by interpolation)
resampler = mlpgw.Transformer(name='resample', new_time=nominal_time)

# Apply resampler to some data
resampled_circ = resampler.transform(**circ)

plt.figure()
plt.plot(circ['time'], circ['a'])
plt.plot(resampled_circ['time'], resampled_circ['a'])

# Create data set randomly
dataset, info = mlpgw.random_dataset(size=1, nominal_time=nominal_time,
                                            random_binary_parameters={'s1': (0,0,0), 's2': (0,0,0), 
                                                                      'mass_ratio_range': (1,10), 'total_mass_range': (20,20)},
                                            waveform_parameters={'model': 'SEOBNRv4'})

print(info)

true_data = mlpgw.CompactBinary(**dataset['0']).get_waveform()
transformer = mlpgw.Transformer(name='bivar2circ', unwrap=True)
true_data = mlpgw.particular_time_align(**transformer.transform(**true_data))

resampler = mlpgw.Transformer(name='resample', new_time=true_data['time'])
data = resampler.transform(**{**dataset['0'], **{'time': nominal_time}})

plt.figure()
plt.plot(true_data['time'], np.real(true_data['a']*np.exp(1j*true_data['phi'])))
plt.plot(true_data['time'], np.real(data['a']*np.exp(1j*data['phi'])))

# Apply a transform to a whole data set
dataset = transformer.inverse_transform_dataset(dataset)

# Add new parameters (possibly forgotten) to dataset without recomputing waveforms
# /!\ You should have in dataset all that allow to compute your new parameters
dataset = mlpgw.add_binary_parameters(dataset=dataset, names=['mass_ratio'])

# /!\ Mistake example : m2 will be default value because m2 is missing from dataset
dataset, info = mlpgw.random_dataset(size=2, nominal_time=nominal_time, parameters_of_interest=['m1'],
                                            waveform_parameters={'inclination': 63*np.pi/128},
                                            random_binary_parameters={'tilt_range': [0.,np.pi/6]},
                                            transformer=mlpgw.Transformer(name='bivar2circ', unicity=False, unwrap=True))

# Save dataset informations
#mlpgw.save_obj(info, 'info')
# Load dataset informations
#test = mlpgw.load_obj('info')

dataset = mlpgw.add_binary_parameters(dataset=dataset, names=['m2'])

# Resample a whole dataset
resampler = mlpgw.Transformer(name='resample', old_time=nominal_time, new_time=true_data['time'][true_data['time']>=-1]) 
resampled_dataset = resampler.transform_dataset(dataset=dataset)

plt.figure()
plt.plot(true_data['time'][true_data['time']>=-1], resampled_dataset['0']['a'])

# Create a pandas DataFrame from dataset
df = pd.DataFrame.from_dict(dataset, 'index')

# Create dataset from pandas DataFrame
d = df.to_dict('index')



