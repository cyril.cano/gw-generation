#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 11:46:13 2021

@author: canocy
"""

#!/usr/bin/env python

import sys
import argparse
import json
from datetime import date
import time

import numpy as np
import pandas as pd
import mlpgw

# ~ 37 sec per waveform

# Usage : python create_dataset.py -b '{"s1": [0,0,1], "s2": [0,1,0]}' -m '{"name": "bivar2circ", "unwrap": true}'

def parser():
    """ Returns an argument parser """
    
    parser = argparse.ArgumentParser(description="Create dataset")

    parser.add_argument("-p", "--path", type=str, 
                        default=str(),
                        help="Path to save the dataset")
    
    parser.add_argument("-n", "--filename", type=str, 
                        default="data-{}".format(date.today().strftime("%y.%m.%d")),
                        help="Name of the dataset")
    
    parser.add_argument("-s", "--size", type=int, 
                        default=0,
                        help="Size of the dataset")
    
    parser.add_argument("-t", "--nominal_time", type=json.loads, 
                        default=mlpgw.DEFAULT_NOMINALTIME_PARAMETERS,
                        help="Description of nominal time to use")
    
    parser.add_argument("-b", "--random_binary_parameters", type=json.loads,
                        default=mlpgw.DEFAULT_RANDOM_COMPACTBINARY_PARAMETERS,
                        help="Description of the binary parameter ranges or values")
    
    parser.add_argument("-w", "--waveform_parameters", type=json.loads,
                        default=mlpgw.DEFAULT_WAVEFORM_PARAMETERS,
                        help="Description of the waveform parameters")
    
    parser.add_argument("-i", "--parameters_of_interest", type=json.loads,
                        default=mlpgw.ALL_COMPACTBINARY_PARAMETERS,
                        help="Description of parameters to get in dataset")
    
    parser.add_argument("-m","--transformer-parameters", type=json.loads,
                        default=mlpgw.DEFAULT_TRANSFORMER_PARAMETERS,
                        help=("Description of transform parameters (model you want)"))
    
#    parser.add_argument("-a","--time-align-parameters", type=json.loads,
#                        default=mlpgw.DEFAULT_TIMEALIGN_PARAMETERS,
#                        help=("Description of transform parameters (model you want)"))
    
    parser.add_argument("--seed", type=int, 
                        default=None,
                        help="Seed for random generator")
    
    return parser

if __name__ == "__main__":
    
    parser = parser()
    args = parser.parse_args(sys.argv[1:])
    
    t0 = time.time()
    dataset, info = mlpgw.random_dataset(size=args.size, 
                                         nominal_time = mlpgw.get_nominal_time(**args.nominal_time), 
                                         transformer = mlpgw.Transformer(**args.transformer_parameters),
#                                         time_align_parameters = args.time_align_parameters,
                                         parameters_of_interest = args.parameters_of_interest, 
                                         waveform_parameters = args.waveform_parameters, 
                                         random_binary_parameters = args.random_binary_parameters,
                                         seed = args.seed)
    t1 = time.time()
    if args.path != str():
        args.path += "/"
    mlpgw.save_obj(dataset, args.path+args.filename)
#    np.savez_compressed(args.path+args.filename, **dataset)
    # Or 
#    df = pd.DataFrame.from_dict(dataset, 'index')
#    df.to_hdf(args.path+args.filename+'.h5', 'data')
    # Or
#    df.drop(mlpgw.ALL_COMPACTBINARY_PARAMETERS, axis=1).to_hdf(args.filename+'.wvf'+'.h5', 'df')
#    df[mlpgw.ALL_COMPACTBINARY_PARAMETERS].to_hdf(args.filename+'.param'+'.h5', 'df')
    t2 = time.time()
    
    mlpgw.save_obj(info, args.path+args.filename+'.info')
    
#    # To read :
#    pd.read_hdf('data.h5', 'data')
#    dataset = df.to_dict('index')
#    # Or
#    npzfile = np.load('data.npz', allow_pickle=True)
#    dataset = {key: npzfile[key] for key in npzfile.files}
    
    print("Wrote {}.\n{} sec to create dataset ({} sec per wvf) and {} sec to save it.".format(args.filename, int(t1-t0), 
                                                                                               int((t1-t0)/args.size), int(t2-t1)))
